﻿using System.Collections;
using System.Collections.Generic;

public class PriorityQueue
{

    private List<(int, Node)> heap;

    public PriorityQueue() {
        heap = new List<(int, Node)>();
    }
    // En düşük derinliğe ship olan ilk sırada yer alır
    public void Enqueue(int depth, Node node) {
        int index = 0;
        int d;
        Node cur;
        for(int i=0; i<heap.Count; i++) {
            (d, cur) = heap[i];
            if (depth < d) {
                index = i;
                break;
            }
        }
        heap.Insert(index, (depth, node));
    }

    // En düşük derinliğe sahip olanı getir, ve heap dan da çıkart
    public (int, Node) Dequeue() {
        (int depth, Node node) = heap[0];
        heap.RemoveAt(0);
        return (depth, node);
    }

    public int Count() {
        return heap.Count;
    }
}
