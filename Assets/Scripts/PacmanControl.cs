﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanControl : MonoBehaviour
{
    private static bool isMoving = false;
    List<Point> path;
    Point onPoint = new Point(0, 0);
    int onid = 0;
    Vector3 currentTarget;
    bool hedefVar = false;

    public Transform reward;

    public float speed = 3;

    public static bool getIsMoving() {
        return isMoving;
    }

    public Point GetOnPoint()
    {
        return onPoint;
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (isMoving)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, currentTarget, Time.deltaTime * speed);
            if (Vector3.Distance(transform.localPosition, currentTarget) < 0.01f)
            {
                onid += 1;
                if (onid == path.Count)
                {
                    isMoving = false;
                    hedefVar = false;

                    reward.gameObject.SetActive(false);

                }
                else
                {
                    onPoint = path[onid];
                    currentTarget = new Vector3(onPoint.x, onPoint.y, 0);
                }
            }
        }
    }

    public void SetTarget(List<Point> path)
    {
        if (isMoving || path == null || path.Count == 0)
            return;
        
        Point lastPoint = path[path.Count - 1];
        if (lastPoint.x == onPoint.x && lastPoint.y == onPoint.y)
            return;

        this.path = path;
        onid = 0;
        //onPoint = path[onid];
        currentTarget = new Vector3(onPoint.x, onPoint.y, 0);
        hedefVar = true;

        //isMoving = true;
    }

    public void goToTarget() {
        if (hedefVar && !isMoving)
            isMoving = true;
    }
}
