﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point
{
    public int x;
    public int y;
    public Point(int y, int x)
    {
        this.y = y;
        this.x = x;
    }
    public int[] getPoint()
    {
        return new int[] { y, x };
    }

    public string toStr()
    {
        return "(" + y + "," + x + ")";
    }

    public bool isEqual(Point other)
    {
        return this.x == other.x && this.y == other.y;
    }
}