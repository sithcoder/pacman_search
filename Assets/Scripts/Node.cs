﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public Point current;
    public List<Point> path;
    public List<Point> visited;
    public Node(Point current, List<Point> path, List<Point> visited)
    {
        this.current = current;
        this.path = path;
        this.visited = visited;
    }
}
