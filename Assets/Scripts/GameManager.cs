﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject kutuObject;
    public GameObject kenaryatayObject;
    public GameObject kenardikeyObject;
    public GameObject okObject;
    public Transform mapOffset;
    public Transform reward;
    public Transform okroot;

    //public Transform okParent;
    public Transform pacman;
    private PacmanControl pacmanControl;

    public int mapWidth = 3;
    public int mapHeight = 4;

    /*private int[,] map = new int[,] {
        { 0b0011, 0b0101, 0b1001 },
        { 0b0010, 0b1001, 0b1010 },
        { 0b1010, 0b1110, 0b1010 },
        { 0b0110, 0b0101, 0b1100 }
    };*/
    private int[,] map = new int[,] {
        { 0b0011, 0b0101, 0b1001, 0b0111, 0b0001, 0b0101, 0b0101, 0b1001 },
        { 0b0010, 0b1001, 0b0010, 0b1001, 0b0110, 0b0001, 0b1101, 0b1010 },
        { 0b1010, 0b1010, 0b0010, 0b0100, 0b1101, 0b1010, 0b0011, 0b1000 },
        { 0b1010, 0b0010, 0b0000, 0b0001, 0b0001, 0b1000, 0b0010, 0b1000 },
        { 0b1010, 0b0011, 0b0101, 0b0100, 0b1101, 0b0010, 0b1100, 0b1010 },
        { 0b0110, 0b0100, 0b0101, 0b0101, 0b0101, 0b0100, 0b0101, 0b1100 },
    };
    private List<Point>[,] graph;

    public static Point hedef;

    void Start()
    {
        pacmanControl = pacman.GetComponent<PacmanControl>();

        reward.gameObject.SetActive(false);

        buildGraph();
        buildMapFromData(); // ekrana map çizmek için
    }

    void Update()
    {
        
    }

    // Hangi node dan hangi nodelara yol olduğunu önceden hesapla
    public void buildGraph()
    {
        if (map == null)
            return;
        if (graph != null)
            return;

        int h = map.GetLength(0);
        int w = map.GetLength(1);
        graph = new List<Point>[h, w];
        for (int y = 0; y < h; y++)
        {
            for (int x = 0; x < w; x++)
            {
                graph[y, x] = new List<Point>();
                if ((x < w - 1) && ((map[y, x] & 8) == 0))
                    graph[y, x].Add(new Point(y, x + 1));
                if ((y < h - 1) && ((map[y, x] & 4) == 0))
                    graph[y, x].Add(new Point(y + 1, x));
                if ((x > 0) && ((map[y, x] & 2) == 0))
                    graph[y, x].Add(new Point(y, x - 1));
                if ((y > 0) && ((map[y, x] & 1) == 0))
                    graph[y, x].Add(new Point(y - 1, x));
            }
        }
    }

    // Ekrana sprite ları çiz
    private void buildMapFromData()
    {
        int h = map.GetLength(0);
        int w = map.GetLength(1);

        for (int y = 0; y < h; y++)
        {
            for (int x = 0; x < w; x++)
            {
                Vector3 pos = new Vector3(x, y, 0);
                GameObject kutu = Instantiate(kutuObject, mapOffset);
                kutu.transform.localPosition = pos;

                if ((map[y, x] & 0b1000) > 0)
                {
                    GameObject kd = Instantiate(kenardikeyObject, kutu.transform);
                    kd.transform.localPosition = new Vector3(0.5f, 0, 0);
                }
                if ((map[y, x] & 0b0100) > 0)
                {
                    GameObject kd = Instantiate(kenaryatayObject, kutu.transform);
                    kd.transform.localPosition = new Vector3(0, 0.5f, 0);
                }
                if ((map[y, x] & 0b0010) > 0)
                {
                    GameObject kd = Instantiate(kenardikeyObject, kutu.transform);
                    kd.transform.localPosition = new Vector3(-0.5f, 0, 0);
                }
                if ((map[y, x] & 0b0001) > 0)
                {
                    GameObject kd = Instantiate(kenaryatayObject, kutu.transform);
                    kd.transform.localPosition = new Vector3(0, -0.5f, 0);
                }
            }
        }
    }

    private bool isListContainPoint(List<Point> array, Point point)
    {
        foreach (Point p in array)
        {
            if (p.x == point.x && p.y == point.y)
                return true;
        }
        return false;
    }

    // Çift yönlü arama algoritması için gerekli
    private List<Point> BirlesikYol(List<Point> first, List<Point> second, Point target)
    {
        string total = "first: ";
        foreach(Point p in first)
            total += p.toStr();
        
        total += " second: ";

        for (int i = second.Count - 1; i > 0; i--) {
            first.Add(second[i]);
            total += second[i].toStr();
        }

        total += " target: " + target.toStr();
        Debug.Log(total);

        first.Add(target);
        return first;
    }

    
    private void OklariCiz(List<Point> path)
    {
        OklariTemizle();
        for (int i=1; i<path.Count-1; i++)
        {
            int dondurecek = 0;
            Point from = path[i];
            Point to = path[i+1];

            if (from.x == to.x)
            {
                if (to.y > from.y)
                    dondurecek = 90;
                else
                    dondurecek = 270;
            }else if (from.y == to.y)
            {
                if (to.x > from.x)
                    dondurecek = 0;
                else
                    dondurecek = 180;
            }

            GameObject yeniok = Instantiate(okObject, okroot);
            yeniok.transform.localPosition = new Vector3(from.x, from.y, 0);
            yeniok.transform.rotation = Quaternion.AngleAxis(dondurecek, Vector3.forward);
        }
    }
    private void OklariTemizle()
    {
        foreach(Transform child in okroot.transform)
            Destroy(child.gameObject);
    }

    //===================================================================================================================== BUTON İŞLEMLERİ

    private Point generateRandomPointInRange(int h, int w) {
        int y = Random.Range(0, h);
        int x = Random.Range(0, w);

        return new Point(y, x);
    }

    // Breadth First Search yap ve hareket et
    public void PacmanBaslatBFS()
    {
        if (PacmanControl.getIsMoving())
            return;

        Point target = generateRandomPointInRange(mapHeight, mapWidth);

        reward.localPosition = new Vector3(target.x, target.y, 0);
        reward.gameObject.SetActive(true);

        Point from = pacmanControl.GetOnPoint();

        List<Point> path = generatePathBFS(from, target);
        pacmanControl.SetTarget(path);

        string pathstr = "from: " + from.toStr()  + "path: ";
        foreach (Point p in path)
            pathstr += p.toStr();
        Debug.Log(pathstr);
        
        OklariCiz(path);
    }

    // Derinlik öncelikli arama yap ve hareket et
    public void PacmanBaslatDFS()
    {
        if (PacmanControl.getIsMoving())
            return;
        
        Point target = generateRandomPointInRange(mapHeight, mapWidth);

        reward.localPosition = new Vector3(target.x, target.y, 0);
        reward.gameObject.SetActive(true);

        Point from = pacmanControl.GetOnPoint();

        List<Point> path = generatePathDFS(from, target);
        pacmanControl.SetTarget(path);

        string pathstr = "from: " + from.toStr()  + "path: ";
        foreach (Point p in path)
            pathstr += p.toStr();
        Debug.Log(pathstr);

        OklariCiz(path);
    }

    // Çift yönlü arama yap ve hareket et
    public void PacmanBaslatBiDirectional()
    {
        if (PacmanControl.getIsMoving())
            return;
        
        Point target = generateRandomPointInRange(mapHeight, mapWidth);

        reward.localPosition = new Vector3(target.x, target.y, 0);
        reward.gameObject.SetActive(true);

        Point from = pacmanControl.GetOnPoint();

        List<Point> path = generatePathBiDirectional(from, target);
        pacmanControl.SetTarget(path);

        string pathstr = "from: " + from.toStr()  + "path: ";
        foreach (Point p in path)
            pathstr += p.toStr();
        Debug.Log(pathstr);

        OklariCiz(path);
    }

    public void PacmanBaslatAStarManhattan() {
        if (PacmanControl.getIsMoving())
            return;
        
        Point target = generateRandomPointInRange(mapHeight, mapWidth);

        reward.localPosition = new Vector3(target.x, target.y, 0);
        reward.gameObject.SetActive(true);

        Point from = pacmanControl.GetOnPoint();


        List<Point> path = generatePathAStarManhattan(from, target);
        pacmanControl.SetTarget(path);

        string pathstr = "from: " + from.toStr()  + "path: ";
        foreach (Point p in path)
            pathstr += p.toStr();
        Debug.Log(pathstr);

        OklariCiz(path);
    }

    public void PacmanBaslatAStarEuclidean() {
        if (PacmanControl.getIsMoving())
            return;
        
        Point target = generateRandomPointInRange(mapHeight, mapWidth);

        reward.localPosition = new Vector3(target.x, target.y, 0);
        reward.gameObject.SetActive(true);

        Point from = pacmanControl.GetOnPoint();


        List<Point> path = generatePathAStarEuclidean(from, target);
        pacmanControl.SetTarget(path);

        string pathstr = "from: " + from.toStr()  + "path: ";
        foreach (Point p in path)
            pathstr += p.toStr();
        Debug.Log(pathstr);

        OklariCiz(path);

    }

















    //===================================================================================================================== HEDEFE GİDEN YOLU HESAPLAYAN ALGORİTMALAR

    // Breadt First Search
    public List<Point> generatePathBFS(Point start, Point target)
    {
        List<Point> path = new List<Point>();
        path.Add(new Point(start.y, start.x));

        PriorityQueue queue = new PriorityQueue();
        queue.Enqueue(0, new Node(start, path, new List<Point>()));

        while(queue.Count() > 0) {
            PriorityQueue queuechildren = new PriorityQueue();
            while(queue.Count() > 0) {
                (int depth, Node node) = queue.Dequeue();

                path = node.path.ConvertAll(point => new Point(point.y, point.x));
                List<Point> children = graph[node.current.y, node.current.x];

                List<Point> visited = node.visited.ConvertAll(point => new Point(point.y, point.x));
                visited.Add(node.current);

                if (target.isEqual(node.current))
                {
                    path.Add(node.current);
                    return path;
                }

                foreach (Point child in children)
                {
                    if (!isListContainPoint(visited, child))
                    {
                        if (target.isEqual(child))
                        {
                            path.Add(child);
                            return path;
                        }

                        int now_depth = path.Count;

                        List<Point> n_path = path.ConvertAll(point => new Point(point.y, point.x));
                        n_path.Add(child);

                        queuechildren.Enqueue(now_depth, new Node(child, n_path, visited));
                    }
                }
            }
            while(queuechildren.Count() > 0) {
                (int depth, Node node) = queuechildren.Dequeue();
                queue.Enqueue(depth, node);
            }
        }

        return path;
    }

    public List<Point> generatePathDFS(Point start, Point target)
    {
        List<Point> path = new List<Point>();
        path.Add(new Point(start.y, start.x));
        
        PriorityQueue queue = new PriorityQueue();
        queue.Enqueue(0, new Node(start, path, new List<Point>()));

        while (queue.Count() > 0) {
            (int depth, Node node) = queue.Dequeue();

            path = node.path.ConvertAll(point => new Point(point.y, point.x));

            List<Point> visited = node.visited.ConvertAll(point => new Point(point.y, point.x));
            visited.Add(node.current);

            if (target.isEqual(node.current))
            {
                path.Add(node.current);
                return path;
            }
            
            // bu node'un alt nodelarını getir
            List<Point> children = graph[node.current.y, node.current.x];

            foreach (Point child in children)
            {
                if (!isListContainPoint(visited, child))
                {
                    if (target.isEqual(child))
                    {
                        path.Add(child);
                        return path;
                    }

                    int now_depth = path.Count;

                    List<Point> n_path = path.ConvertAll(point => new Point(point.y, point.x));
                    n_path.Add(child);

                    queue.Enqueue(-now_depth, new Node(child, n_path, visited));
                }
            }
        }

        return path;
    }

    // Çift yönlü arama algoritması
    public List<Point> generatePathBiDirectional(Point start, Point target)
    {
        /*
        List<Point> path = new List<Point>();
        
        PriorityQueue queueu = new PriorityQueue();
        queueu.Enqueue(0, new Node(start, path, new List<Point>()));
        
        PriorityQueue queued = new PriorityQueue();
        queued.Enqueue(0, new Node(target, path, new List<Point>()));

        while(queueu.Count() > 0 || queued.Count() > 0) {
            if (queueu.Count() > 0) {
                (int depth, Node node) = queueu.Dequeue();

                List<Point> children = graph[node.current.y, node.current.x];

                List<Point> visited = node.visited.ConvertAll(point => new Point(point.y, point.x));
                visited.Add(node.current);

                if (target.isEqual(node.current))
                {
                    Debug.Log("1");
                    path.Add(node.current);
                    return path;
                }

                foreach (BFSNode n in aktiflerd)
                {
                    if (node.current.isEqual(n.current))
                    {
                        // çakışma var yol bitti, birleşik yolu oluştur ve dönder
                        n.path.RemoveAt(n.path.Count - 1);
                        //node.path.Add(node.current);
                        Debug.Log("2");
                        return BirlesikYol(node.path, n.path, target);
                    }
                }
            }
        }

        return path;*/

        List<Point> path = new List<Point>();
        path.Add(start);

        List<Point> pathd = new List<Point>();
        pathd.Add(target);


        List<Node> aktifleru = new List<Node>();
        List<Node> aktiflerd = new List<Node>();

        aktifleru.Add(new Node(start, path, new List<Point>()));
        aktiflerd.Add(new Node(target, pathd, new List<Point>()));

        while (aktifleru.Count > 0 || aktiflerd.Count > 0)
        {
            List<Node> yeniau = new List<Node>();
            List<Node> yeniad = new List<Node>();

            foreach (Node node in aktifleru)
            {
                path = node.path.ConvertAll(point => new Point(point.y, point.x));
                List<Point> children = graph[node.current.y, node.current.x];

                List<Point> visited = node.visited.ConvertAll(point => new Point(point.y, point.x));
                visited.Add(node.current);

                if (target.isEqual(node.current))
                {
                    Debug.Log("1");
                    path.Add(node.current);
                    return path;
                }

                foreach (Node n in aktiflerd)
                {
                    if (node.current.isEqual(n.current))
                    {
                        // çakışma var yol bitti, birleşik yolu oluştur ve dönder
                        n.path.RemoveAt(n.path.Count - 1);
                        //node.path.Add(node.current);
                        Debug.Log("2");
                        return BirlesikYol(node.path, n.path, target);
                    }
                }

                foreach (Point child in children)
                {
                    if (!isListContainPoint(visited, child))
                    {
                        if (target.isEqual(child))
                        {
                            Debug.Log("3");
                            path.Add(child);
                            return path;
                        }


                        foreach (Node n in aktiflerd)
                        {
                            if (child.isEqual(n.current))
                            {
                                // çakışma var yol bitti, birleşik yolu oluştur ve dönder
                                n.path.RemoveAt(n.path.Count - 1);
                                Debug.Log("4");
                                path.Add(child);
                                return BirlesikYol(path, n.path, target);
                            }
                        }



                        List<Point> n_path = path.ConvertAll(point => new Point(point.y, point.x));
                        n_path.Add(child);

                        yeniau.Add(new Node(child, n_path, visited));

                    }
                }
            }

            foreach (Node node in aktiflerd)
            {
                path = node.path.ConvertAll(point => new Point(point.y, point.x));
                List<Point> children = graph[node.current.y, node.current.x];

                List<Point> visited = node.visited.ConvertAll(point => new Point(point.y, point.x));
                visited.Add(node.current);

                if (start.isEqual(node.current))
                {
                    return BirlesikYol(new List<Point>(), path, target);
                }

                foreach (Node n in aktifleru)
                {
                    if (node.current.isEqual(n.current))
                    {
                        // çakışma var yol bitti, birleşik yolu oluştur ve dönder
                        n.path.RemoveAt(n.path.Count - 1);
                        Debug.Log("5");
                        return BirlesikYol(n.path, node.path, target);
                    }
                }

                foreach (Point child in children)
                {
                    if (!isListContainPoint(visited, child))
                    {
                        if (start.isEqual(child))
                        {
                            Debug.Log("6");
                            path.Add(child);
                            return BirlesikYol(new List<Point>(), path, target);
                        }

                        List<Point> n_path = path.ConvertAll(point => new Point(point.y, point.x));
                        n_path.Add(child);

                        yeniad.Add(new Node(child, n_path, visited));
                    }
                }
            }

            aktifleru = yeniau;
            aktiflerd = yeniad;

        }

        Debug.Log("YOL BULUNAMADI");
        return path;
    }


    private int ManhattanNumber(Point start, Point target) {
        return Mathf.Abs(start.x - target.x) + Mathf.Abs(start.y - target.y);
    }

    // A* algoritması, Manhattan uzaklığı ile uygulanması
    // Hedef konum ile pacman'in başlangıç konumunun yatay uzaklığı ile dikey uzaklığı toplamını kullanacağız
    public List<Point> generatePathAStarManhattan(Point start, Point target) {
        List<Point> path = new List<Point>();
        path.Add(start);
        int h3 = ManhattanNumber(start, target);

        PriorityQueue queue = new PriorityQueue();
        queue.Enqueue(0 + h3, new Node(start, path, new List<Point>()));

        while (queue.Count() > 0) {
            (int depth, Node node) = queue.Dequeue();

            path = node.path.ConvertAll(point => new Point(point.y, point.x));

            List<Point> visited = node.visited.ConvertAll(point => new Point(point.y, point.x));
            visited.Add(node.current);

            if (target.isEqual(node.current))
            {
                path.Add(node.current);
                return path;
            }

            // bu node'un alt nodelarını getir
            List<Point> children = graph[node.current.y, node.current.x];

            foreach (Point child in children)
            {
                if (!isListContainPoint(visited, child))
                {
                    if (target.isEqual(child))
                    {
                        path.Add(child);
                        return path;
                    }

                    int now_depth = path.Count;
                    int h4 = ManhattanNumber(child, target);

                    List<Point> n_path = path.ConvertAll(point => new Point(point.y, point.x));
                    n_path.Add(child);

                    queue.Enqueue(now_depth + h4, new Node(child, n_path, visited));
                }
            }
        }

        Debug.Log("Manhattan Değeri: " + h3);

        return path;
    }

    private int EuclidienNumber(Point start, Point target) {
        int diffx = Mathf.Abs(start.x - target.x);
        int diffy = Mathf.Abs(start.y - target.y);

        return (int)Mathf.Sqrt(diffx * diffx + diffy * diffy);
    }
    // A* algoritması, Euclidean uzaklığı (Kuş uçuşu mesafe, z = (x*x + y*y) ^ 1/2 ) ile uygulanması
    // Hedef konum ile pacman'in başlangıç konumunun arasındaki kuş uçuşu mesafe.
    public List<Point> generatePathAStarEuclidean(Point start, Point target) {
        List<Point> path = new List<Point>();
        path.Add(start);
        int h3 = EuclidienNumber(start, target);

        PriorityQueue queue = new PriorityQueue();
        queue.Enqueue(0 + h3, new Node(start, path, new List<Point>()));

        while (queue.Count() > 0) {
            (int depth, Node node) = queue.Dequeue();

            path = node.path.ConvertAll(point => new Point(point.y, point.x));

            List<Point> visited = node.visited.ConvertAll(point => new Point(point.y, point.x));
            visited.Add(node.current);

            if (target.isEqual(node.current))
            {
                path.Add(node.current);
                return path;
            }

            // bu node'un alt nodelarını getir
            List<Point> children = graph[node.current.y, node.current.x];

            foreach (Point child in children)
            {
                if (!isListContainPoint(visited, child))
                {
                    if (target.isEqual(child))
                    {
                        path.Add(child);
                        return path;
                    }

                    int now_depth = path.Count;
                    int h4 = EuclidienNumber(child, target);

                    List<Point> n_path = path.ConvertAll(point => new Point(point.y, point.x));
                    n_path.Add(child);

                    queue.Enqueue(now_depth + h4, new Node(child, n_path, visited));
                }
            }
        }

        Debug.Log("Manhattan Değeri: " + h3);

        return path;
    }

}
